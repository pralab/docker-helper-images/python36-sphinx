FROM python:3.6

RUN apt-get update && apt-get install -y -q texlive-latex-extra dvipng wget
RUN TEMP_DEB="$(mktemp)" \
  && wget -O "$TEMP_DEB" 'https://github.com/jgm/pandoc/releases/download/2.13/pandoc-2.13-1-amd64.deb' \ 
  && dpkg -i "$TEMP_DEB" \
  && rm -f "$TEMP_DEB"
